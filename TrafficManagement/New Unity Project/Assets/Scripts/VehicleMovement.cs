﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{
    public Vector3 acceleration; //Direction
    public Vector3 velocity; //Movement
    public Vector3 direction;
    public float accelerationFactor;
    public float speed;
    public string state = "Green";

    Vector3 xPos = new Vector2(30f, 0);
    public float topSpeed = 30f;

    private Vector3 position = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        velocity = Vector3.zero;
        acceleration = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.right * position.x * Time.deltaTime;
        State();
        Acceleration();
        ScreenLoop();

    }

    void Acceleration()
    {

        acceleration /= accelerationFactor;
        velocity += acceleration;
        velocity.x = Mathf.Clamp(velocity.x, 0.0f, topSpeed);
        position.x = velocity.x;
    }

    void State()
    {
        Clicked();
        Motion();
        //Debug.Log("State: " + state);
    }

    void Motion()
    {
        if (state == "Green") //Going Right Panel //green or moving
        {
            acceleration.x = speed;
            direction.x = 0.1f;
            //Debug.Log("KeyPress W");
        }

        if (state == "Bumper") //Loading/Unloading
        {
            acceleration.x = 0.0f;

            if (velocity.x > direction.x)
            {
                velocity.x -= direction.x * 2.45f;

                if (velocity.x < direction.x)
                {
                    direction.x = 0.0f;
                }
            }
            StartCoroutine(Load());
        }

        if (state == "Slow") //Clicked or Slowing Down
        {
            acceleration.x = 0.0f;

            if (velocity.x > direction.x)
            {
                velocity.x -= direction.x * 2.45f;

                if (velocity.x < direction.x)
                {
                    direction.x = 0.0f;
                }
            }
        }
    }

    IEnumerator Load()
    {
        yield return new WaitForSeconds(5.0f);
        state = "Green";
    }


    void ScreenLoop()
    {
        if (this.transform.position.x > 15)
        {
            this.transform.position += -xPos;
        }
    }

    void Clicked()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

            if (hit.collider)
            {
                state = "Bumper";
            }
        }
    }
}