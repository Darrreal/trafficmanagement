﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WorldState
{
    Wander,
    Interact
}

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    public static PlayerController Instance;

    public WorldState CurState = WorldState.Wander;

    private readonly float step = 6.0f;
    
    //private GameManager gameManager;
    private Rigidbody2D rb;
    private Vector2 origScale;

    private Vector2 mapBound = new Vector2(7.7f, 3.5f);

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        //gameManager = GameManager.Instance;
        rb = GetComponent<Rigidbody2D>();
        origScale = transform.localScale;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //if (gameManager.CurState != GameState.Start) return;

        Movement();
        if (rb.velocity != Vector2.zero) rb.velocity = Vector2.zero;

    }

    private void Update()
    {
        CheckEdges();
    }

    void Movement()
    {
        float speed = step * Time.deltaTime;
        Vector3 dir = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            dir += transform.up * speed;
        }
        if (Input.GetKey(KeyCode.S))
        {
            dir -= transform.up * speed;
        }
        if (Input.GetKey(KeyCode.A))
        {
            dir -= transform.right * speed;
            transform.localScale = new Vector2(-origScale.x, transform.localScale.y);
            
        }
        if (Input.GetKey(KeyCode.D))
        {
            dir += transform.right * speed;
            transform.localScale = new Vector2(origScale.x, transform.localScale.y);
        }

        //Move
        transform.Translate(dir);
    }

    void CheckEdges()
    {
        Vector2 localPosition = transform.position;
        localPosition.x = Mathf.Clamp(localPosition.x, -mapBound.x, mapBound.x);
        localPosition.y = Mathf.Clamp(localPosition.y, -mapBound.y, mapBound.y);

        transform.position = localPosition;
    }
}
