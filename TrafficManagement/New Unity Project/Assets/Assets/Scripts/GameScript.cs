﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScript : MonoBehaviour
{
    public GameObject Jeep;
    public GameObject Truck;

    public GameObject lane1;
    public GameObject lane2;
    public GameObject lane3;
    //public GameObject lane4;

    private int jeepSpawnCooldown = 80;
    private int jeepSpawnCooldownMax = 100;

    private int truckSpawnCooldown = 40;
    private int truckSpawnCooldownMax = 100;

    public int randSpot;

    private List<GameObject> listOfJeeps = new List<GameObject>();
    private List<GameObject> listOfTrucks = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        jeepSpawnCooldown++;
        if(jeepSpawnCooldown>jeepSpawnCooldownMax && Random.Range(1, 50) == 1)
        {
            jeepSpawnCooldownMax = Random.Range(70, 100);
            //Debug.Log("spawn random jeep");
            jeepSpawnCooldown = 0;

            int randomSpawn = Random.Range(0, 2);
            GameObject currentLane = lane1;
            if (randomSpawn == 0)
            {
                currentLane = lane2;
            }
            else
            {
                currentLane = lane1;
            }

            GameObject newJeep = Jeep;
            bool makeNewJeep = true;
            for(int i = 0; i < listOfJeeps.Count; i++)
            {
                if (listOfJeeps[i].activeSelf == false)
                {
                    makeNewJeep = false;
                    newJeep = listOfJeeps[i];
                    newJeep.transform.position = currentLane.transform.position;
                    newJeep.SetActive(true);
                    break;
                }
            }
            if (makeNewJeep == true)
            {
                //randSpot = Random.Range(0, 2);
                //if (randSpot == 0)
                //{
                    newJeep = Instantiate(Jeep, currentLane.transform.position, Quaternion.identity);
                //}
                /*if (randSpot == 1)
                {
                    newJeep = Instantiate(Jeep, new Vector3(9.89f, currentLane.transform.position.y, currentLane.transform.position.z), Quaternion.identity);
                }*/
                listOfJeeps.Add(newJeep);
                newJeep.SetActive(true);
            }

            if (randomSpawn == 0)
            {
                newJeep.GetComponent<VehicleMovement>().currentLane = 1;
            }
            else
            {
                newJeep.GetComponent<VehicleMovement>().currentLane = 0;
            }
        }

        truckSpawnCooldown++;
        if (truckSpawnCooldown > truckSpawnCooldownMax && Random.Range(1, 50) == 1)
        {
            truckSpawnCooldownMax = Random.Range(70, 100);
            //Debug.Log("spawn random jeep");
            truckSpawnCooldown = 0;

            int randomSpawn = Random.Range(0, 2);
            GameObject currentLane = lane1;
            if (randomSpawn == 0)
            {
                currentLane = lane1;
            }
            else
            {
                currentLane = lane2;
            }

            GameObject newTruck = Truck;
            bool makeNewTruck = true;
            for (int i = 0; i < listOfTrucks.Count; i++)
            {
                if (listOfTrucks[i].activeSelf == false)
                {
                    makeNewTruck = false;
                    newTruck = listOfTrucks[i];
                    newTruck.transform.position = currentLane.transform.position;
                    newTruck.SetActive(true);
                    break;
                }
            }
            if (makeNewTruck == true)
            {
                newTruck = Instantiate(Truck, currentLane.transform.position, Quaternion.identity);
                listOfTrucks.Add(newTruck);
                newTruck.SetActive(true);
            }

            if (randomSpawn == 0)
            {
                newTruck.GetComponent<VehicleMovement>().currentLane = 0;
            }
            else
            {
                newTruck.GetComponent<VehicleMovement>().currentLane = 1;
            }
        }
    }


}
