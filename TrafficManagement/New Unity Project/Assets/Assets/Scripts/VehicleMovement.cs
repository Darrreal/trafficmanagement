﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{
    private GameObject playerHealth;
    public Vector3 acceleration; //Direction
    public Vector3 velocity; //Movement
    public Vector3 direction;
    public float accelerationFactor;
    public float speedMultiplier;
    private float extraSpeed = 0;
    public int currentLane;
    public string state = "Green";

    public GameObject lane1;
    public GameObject lane2;
    public List<GameObject> lanes = new List<GameObject>();
    public bool turning = false;
    public bool turnUp = false;
    public bool turnDown = false;
    
    public float topSpeed = 10f;

    private int randomSpawn;

    private Vector3 position = Vector3.zero;

    private int randomLaneCooldownCap = 300;
    private int randomLaneCooldown = 0;

    private int randomSpeedingCooldownCap = 400;
    private int randomSpeedingCooldown = 0;

    private Vector3 screenPoint;
    private Vector3 offset;

    private Health health;
    private bool isChecked;

    private static VehicleMovement _instance;

    public static VehicleMovement Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<VehicleMovement>();
            }
            return _instance;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        velocity = new Vector3(3.0f, 0.0f, 0.0f);
        acceleration = Vector3.zero;

        lane1 = GameObject.Find("Lanes").transform.GetChild(0).gameObject;
        lane2 = GameObject.Find("Lanes").transform.GetChild(1).gameObject;
        lanes.Add(lane1);
        lanes.Add(lane2);

        playerHealth = GameObject.Find("GameRunner");
        health = playerHealth.GetComponent<Health>();

        isChecked = false;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.right * position.x * Time.deltaTime;
        State();
        Acceleration();
        ScreenLoop();
        
        GameObject desiredLane = lanes[currentLane];
        if(transform.position.y < desiredLane.transform.position.y - 0.3)
        {
            turning = true;
            turnUp = true;
            turnDown = false;
        }
        else if (transform.position.y > desiredLane.transform.position.y + 0.3)
        {
            turning = true;
            turnDown = true;
            turnUp = false;
        }
        else
        {
            //Debug.Log("correct");
            turning = false;
            turnUp = false;
            turnDown = false;
        }
        TurnVehicle();

        RandomChangeLane();
        RandomSpeeding();
        CheckEdges();
    }

    void Acceleration()
    {

        acceleration /= accelerationFactor;
        velocity += acceleration;
        velocity.x = Mathf.Clamp(velocity.x, 0.0f, topSpeed);
        position.x = velocity.x;
    }

    void State()
    {
        //Clicked();
        Motion();
        
        //Debug.Log("State: " + state);
    }

    void TurnVehicle()
    {
        if (state == "Green")
        {
            if (turning == true)
            {
                if (turnUp == true)
                {
                    //Debug.Log("go up");
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 20), 3.0f * Time.deltaTime * (velocity.x / topSpeed));
                }
                else if (turnDown == true)
                {
                    //Debug.Log("go down");
                    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, -20), 3.0f * Time.deltaTime * (velocity.x / topSpeed));
                }
            }
            else
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 0), 7.0f * Time.deltaTime * (velocity.x / topSpeed));
            }
        }
    }

    void RandomChangeLane()
    {
        randomLaneCooldown++;
        float randomBehavior = Random.Range(1, 100);
        if (randomBehavior == 1 && randomLaneCooldown>randomLaneCooldownCap)
        {
            randomLaneCooldown = 0;
            if (currentLane == 0)
            {
                currentLane = 1;
            }
            else
            {
                currentLane = 0;
            }
        }
    }

    void RandomSpeeding()
    {
        randomSpeedingCooldown++;
        float randomBehavior = Random.Range(1, 150);
        if (randomBehavior == 1 && randomSpeedingCooldown > randomSpeedingCooldownCap)
        {
            randomSpeedingCooldown = 0;
            Debug.Log("vehicle now speeding!");
            extraSpeed = 20;
        }
    }

    void Motion()
    {
        if (state == "Green") //Going Right Panel //green or moving
        {
            acceleration.x = speedMultiplier + extraSpeed;
            direction.x = 0.1f;
            //Debug.Log("KeyPress W");
        }

        if (state == "Bumper") //Loading/Unloading
        {
            acceleration.x = 0.0f;

            if (velocity.x > direction.x)
            {
                velocity.x -= direction.x * 2.45f;

                if (velocity.x < direction.x)
                {
                    direction.x = 0.0f;
                }
            }
            StartCoroutine(Load());
        }

        if (state == "Slow") //Clicked or Slowing Down
        {
            acceleration.x = 0.0f;

            if (velocity.x > direction.x)
            {
                velocity.x -= direction.x * 2.45f;

                if (velocity.x < direction.x)
                {
                    direction.x = 0.0f;
                }
            }
        }
    }

    IEnumerator Load()
    {
        yield return new WaitForSeconds(2.0f);
        state = "Green";
    }


    void ScreenLoop()
    {
        if (this.transform.position.x > 15)
        {
            /*float newY = -1.52f;
            randomSpawn = Random.Range(0, 2);
            if (randomSpawn == 0)
            {
                currentLane = 0;
                newY = -1.52f;
                this.transform.position = lane2.transform.position;
            }
            else
            {
                currentLane = 1;
                newY = 1.52f;
                this.transform.position = lane1.transform.position;
            }*/
            this.gameObject.SetActive(false);
            acceleration = new Vector3(0, 0, 0);
            velocity = new Vector3(3.0f, 0, 0);
            extraSpeed = 0;
            transform.rotation = Quaternion.Euler(0, 0, 0);
            randomSpeedingCooldown = 0;
            randomLaneCooldown = 0;
        }
    }

    //void Clicked()
    //{
    //    if (Input.GetMouseButtonDown(0))
    //    {
    //        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    //        Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
    //        RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

    //        if (hit.collider)
    //        {
    //            this.state = "Bumper";
    //        }
    //    }
    //}

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        //transform.position = curPosition;

        if (curPosition.y > gameObject.transform.position.y)
        {
            currentLane = 1;
        }

        if (curPosition.y < gameObject.transform.position.y)
        {
            currentLane = 0;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (this.tag == "Jeep")
        {
            health.numOfHearts--;
        }
        //Destroy(this.gameObject);
        //Destroy(collision.gameObject);
        this.gameObject.SetActive(false);
        collision.gameObject.SetActive(false);
        

        Debug.Log("Crashed");
    }

    void CheckEdges()
    {
        if (this.transform.position.x > 9.1f && isChecked == false)
        {
            if (this.tag == "Jeep" && currentLane == 0)
            {
                health.numOfHearts--;
                Debug.Log("Wrong Lane");
            }

            if (this.tag == "Truck" && currentLane == 1)
            {
                health.numOfHearts--;
                Debug.Log("Wrong Lane");
            }
            isChecked = true;
        }
            //Destroy(this.gameObject);
        
        if (this.transform.position.x > 9.5f)
        {
            //Destroy(this.gameObject);
        }   
        
    }
}